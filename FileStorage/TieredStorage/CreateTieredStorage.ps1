﻿# List Physical Disks

Get-PhysicalDisk


# Set Physical Disk type (SSD)

Set-PhysicalDisk PhysicalDisk1 -MediaType SSD


# Set Physical Disk type (HDD)

Set-PhysicalDisk PhysicalDisk2 -MediaType HDD

# Create a mirror space with storage tiers


$SSD = Get-StorageTier -FriendlyName *SSD*
$HDD = Get-StorageTier -FriendlyName *HDD*
Get-StoragePool CompanyData | New-VirtualDisk -FriendlyName "UserData01" -ResiliencySettingName "Mirror" -StorageTiers $SSD, $HDD -StorageTierSizes 8GB, 32GB

