﻿# Create Country claim type from ( c ) source attribute - Suggested Values (US, JP)
New-ADClaimType country -SourceAttribute c -SuggestedValues:@((New-Object Microsoft.ActiveDirectory.Management.ADSuggestedValueEntry("US","US","")), (New-Object Microsoft.ActiveDirectory.Management.ADSuggestedValueEntry("JP","JP","")))  

# Create Department claim type from ( department) source attribute
New-ADClaimType department -SourceAttribute department