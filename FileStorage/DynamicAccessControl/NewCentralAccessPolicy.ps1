﻿# Update FSRM Classification Properties
Update-FsrmClassificationPropertyDefinition

# Create Central Access Rule (CAR)

New-ADCentralAccessRule


# Create Central Access Policy (CAP)

New-ADCentralAccessPolicy


# Add CAR to CAP

Add-ADCentralAccessPolicyMember

