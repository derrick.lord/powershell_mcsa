﻿# Create Country resource propery 
New-ADResourceProperty Country -IsSecured $true -ResourcePropertyValueType MS-DS-MultivaluedChoice -SharesValuesWith country  


# Enable Deparment_MS resource property
Set-ADResourceProperty Department_MS -Enabled $true  


# Add County to"Global Resoure Property List" as a list memeber
Add-ADResourcePropertyListMember "Global Resource Property List" -Members Country  


# Add Department_MS to"Global Resoure Property List" as a list memeber
Add-ADResourcePropertyListMember "Global Resource Property List" -Members Department_MS