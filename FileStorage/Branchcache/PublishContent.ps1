﻿# Prestage Content Metadata
# Prepare metadata for subsequent export

Publish-BCFileContent -StageData D:\Share1 -StagingPath D:\Staging


# Export File Content Package

Export-BCCachePackage -Destination D:\Export D:\Staging

# Import File Content Package

Import-BCCachePackage D:\PeerDistPackage.zip


