﻿# Dfsrdiag is a command line utility for troubleshooting and forcing replication that uses DFSR
# /partner = Source domain controller
# /member = Destination domain controller
# /time = How long existing replication schedule will be ignored

dfsrdiag syncnow /partner:DC1 /member:DC2 /rgname:"Domain System Volume" /time:!