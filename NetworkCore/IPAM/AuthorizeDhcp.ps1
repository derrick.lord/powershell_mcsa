﻿# Obtain list of currently authorized DHCP Servers
Get-DhcpServerInDC 

# Authorize a DHCP server in AD
# Usage: Add-DhcpServerInDC ServerName
Add-DhcpServerInDC

# To Be automatically discovered by IPAM, a server must host at least
# one DHCP scope. 
# The scope does not have to be activated 

# IPAM Requirements
# 1. Never installed on Domain Controller
# 2. Do not instll on DHCP server