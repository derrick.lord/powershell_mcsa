﻿# Disable DNS Recursion

Set-DnsServerRecursion -Enable $False


# Enable DNS Recursion

Set-DnsServerRecursion -Enable $True


# Remove DNS Root Hints

Get-DnsServerRootHInt | Remove-DnsServerRootHint