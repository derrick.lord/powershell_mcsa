﻿# Enable netmask ordering for /22 subnet
# 0x000003FF is a 32-bit hexadecimalnumber in which the first 22 bits are 0's and the last 10 bits are 1's

dnscmd /config /LocalNetPriorityNetMask 0x000003FF

