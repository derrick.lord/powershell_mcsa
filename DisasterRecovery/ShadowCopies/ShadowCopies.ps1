﻿# Remove Shadow Copies

vssadmin delete shadows /for=D: /all


# Add Shadow Storage

vssadmin add shadowstorage


# List Shadow Volumes

vssadmin list volumes


# List Shadow Copies

vssadmin list shadows