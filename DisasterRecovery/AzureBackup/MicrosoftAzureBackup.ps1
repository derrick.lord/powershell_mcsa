﻿# Register a server with Microsoft
Start-OBRegistration


# Define an encryption passphrase
Set-OBMachineSetting

# Create a new MAB Policy
New-OBPolicy

# MAB policu includes:
# 1. Schedule
# 2. Retention Policy
# 3. File specification

