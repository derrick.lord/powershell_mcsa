﻿# Mount boot.wim image @ C:\Mount

dism /mount-image /ImageFile:C:\WinSxS\Sources\boot.wim /index:1 /MountDir:C:\Mount

# Edit the C:\Mount\Widnows\System32\startnet.cmd
# startnet.cmd - (default) contains a single command "Wpeinit"

# UnMount boot.wim - saving changes

dism /unmount-image /MountDir:C:\Mount /commit