﻿# Cluster Aware Updating (CAU)
# Two Modes:
# 1. self-updating (automatic)
# 2. remote-updating (manual) - no scheduling feature, can use Task Scheduler

# Run CAU (manual)
Invoke-CauRun 

# Install CAU Update Coordinator (auto-updating)
Add-CauClusterRole



