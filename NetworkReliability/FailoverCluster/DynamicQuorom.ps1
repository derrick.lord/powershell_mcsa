﻿# Disable Dynamic Quorum (Static Quorum)

(Get-Cluster).DynamicQuorum = 0 

# Enable Dynamic Quorum

(Get-Cluster).DynamicQuorum = 1